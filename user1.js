module.exports = function(sequelize, DataTypes) 
{
    return sequelize.define('user1', {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true
            },
        name: DataTypes.STRING
        },
        {
            freezeTableName: true
        },
       { tableName : 'user1'}
    )  
}
